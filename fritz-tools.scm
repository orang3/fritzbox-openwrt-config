;;; Copyright © 2020 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (fritz-tools)
  #:use-module (gnu packages python)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix build-system copy)
  #:use-module (guix utils))

(define-public fritz-tools
  (let ((version "1.2")
        (revision "0")
        (commit "813e09c3f58e93541cde5ec4d3f97af0216c64bf"))
    (package
      (name "fritz-tools")
      (version (git-version version revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/freifunk-darmstadt/fritz-tools")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "1fkl5iqncbinsa1xvkkknpaxzxmcic5jbv43wpljaszl18jlwnyp"))))
      (build-system copy-build-system)
      (inputs
       `(("python" ,python)))
      (arguments
       `(#:install-plan
         '(("fritzflash.py" "bin/fritzflash"))))
      (home-page "https://fritz-tools.readthedocs.io")
      (synopsis "Convenience script to flash Fritz!Box devices")
      (description "This package provides @code{fritzflash}, a script
that automates the process of flashing Gluon and OpenWrt to a Fritz!Box
device.")
      (license license:gpl3))))

fritz-tools
