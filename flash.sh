#!/usr/bin/env bash
set -euo pipefail

VER="23.05.5"

url="http://downloads.openwrt.org/releases/${VER}/targets/ath79/generic/openwrt-${VER}-ath79-generic-avm_fritz4020-squashfs-sysupgrade.bin"
here="$(dirname "$0")"
img_bin="${here}/img.bin"

download-image () {
    echo "Downloading firmware..."
    status_code=$(guix shell curl -- curl -L -s -o "$img_bin" -w "%{http_code}" "$url")

    if (( $status_code >= 400 )); then
        echo "Error in downloading firmware, please visit https://openwrt.org/toh/avm/fritz.box.4020 to download the right binary."
        exit 1
    fi

    echo "Successfully downloaded OpenWrt ${VER} for the Fritz!Box 4020!"
}

[[ ! -f "$img_bin" ]] && download-image

guix shell -f fritz-tools.scm -- fritzflash --ip 192.168.178.1 --image "$img_bin"
